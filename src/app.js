document.addEventListener("DOMContentLoaded", function () {
  var $check = setInterval(function () {
    if (typeof window.jQuery == "function") {
      console.log("got $");
      init();
      stopTimer();
    } else {
      console.log("no $");
    }
  }, 200);

  function stopTimer() {
    clearInterval($check);
  }

  function init() {
    let $zoom;

    // reroute the language select to go to this page instead of the homepage on locale change
    $(".lang-dropdown-list li").each(function () {
      var $this = $(this);
      var href = $this.find("a").attr("href");
      $this
        .find("a")
        .attr("href", href.replace("/home", window.location.pathname));
    });

    // main video play button
    $(".video-container button").on("click", function () {
      // overlay for video with scrim
      var vHtml = `<div class="video-modal">
        <div class="scrim">&nbsp;</div>
        <div class="modal-content">
          <div class="icon-close">&#10005;</div>
          <div class="video-container">
          </div>
        </div>
      </div>`;
      var $modal = $(vHtml);
      // get the scren dimensions so we can set the iframe params accordingly

      var videoW = "630";
      var videoH = "354";
      if ($(window).width() <= 650) {
        var ratio = videoH / videoW;
        videoW = $(window).width() * 0.9;
        videoH = videoW * ratio;
      }

      var videoUrl =
        "https://videos.sproutvideo.com/embed/069dddb11d1be7c48f/da9ac4e9f4397b6b";

      if ($(".eco-wrapper").hasClass("ca")) {
        videoUrl =
          "https://videos.sproutvideo.com/embed/a79dddb11d1be6c02e/60c9de463104ccde";
      }

      var $video = $(
        "<iframe class='sproutvideo-player' src='" +
          videoUrl +
          "' width='" +
          videoW +
          "' height='" +
          videoH +
          "' frameborder='0' allowfullscreen autoplay></iframe>"
      );
      $modal.find(".video-container").append($video);
      // click listener for scrim and close button
      $("body").on(
        "click",
        ".video-modal .skrim, .video-modal .icon-close",
        function () {
          $(".video-modal").remove();
        }
      );
      $("body").append($modal);
    });

    // Show Carousel
    $(".shoe-carousel").slick({
      infinite: true,
      mobileFirst: true,
      dots: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: "unslick",
        },
      ],
    });

    const $buyButton = $(".eco-buy-now");

    // Handle URL change on mobile when carousel slides
    $(".shoe-carousel").on("afterChange", function (slick, currentSlide) {
      const $currentSlide = $(".shoe-carousel .slick-active img");
      const newUrl = $currentSlide.data("url");
      $buyButton.attr("href", newUrl);
    });

    // Card Carousel
    $(".card-carousel").slick({
      infinite: false,
      mobileFirst: true,
      dots: false,
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: "unslick",
        },
      ],
    });

    // Handle Shoe Preview
    $(".shoe-carousel img").on("click", function () {
      if (window.innerWidth < 1024) return;
      const $this = $(this);
      // clone image
      const $newImage = $this.clone();

      if ($zoom && typeof $zoom.destroy == "function") {
        console.log("destroy");
        $zoom.destroy();
      } else {
        console.log("no destroy");
      }

      // grab current preview image style type
      const style = $(".large-preview img").data("style");

      // Hide / Show necessary images
      $(`img[data-style='${style}'`).show();
      $(".large-preview img").replaceWith($newImage);
      $this.hide();
      $zoom = $(".large-preview img").magnify();

      // Update Buy Button URL
      const newUrl = $this.data("url");
      $buyButton.attr("href", newUrl);
    });

    $(".hover-image").on("mouseenter", function () {
      const $this = $(this);
      const oldImage = $this.attr("src");
      const newImage = $this.data("hover");
      $this.attr("src", newImage);
      $this.data("hover", oldImage);
    });

    $(".hover-image").on("mouseleave", function () {
      const $this = $(this);
      const oldImage = $this.attr("src");
      const newImage = $this.data("hover");
      $this.attr("src", newImage);
      $this.data("hover", oldImage);
    });

    let $baseImage;
    var macroPoster = $("#macro-video").data("poster");
    var macroVideo = $("#macro-video").data("video");
    // Video on Hover for Rubber bottom
    $("body").on("mouseenter", ".video-hover", function () {
      console.log("inserting video");
      const $video = $(
        '<video class="hover-video" src="' +
          macroVideo +
          '" autoplay loop muted poster="' +
          macroPoster +
          '"></video>'
      );
      $baseImage = $(this);
      $baseImage.replaceWith($video);
    });

    $("body").on("mouseleave", ".hover-video", function () {
      $(this).replaceWith($baseImage);
    });

    // Future Section Animation

    $(document).on("scroll", function () {
      // Global scroll position var
      const scrollPos = $(window).scrollTop();
      const bottomScrollPos = scrollPos + $(window).height();

      // handling reveal of cards in plastics section
      const cardScrollPos = $("ul.card-carousel").offset().top;

      if (
        scrollPos > cardScrollPos - $(window).height() * 0.65 &&
        $(window).width() >= 1024
      ) {
        $("ul.card-carousel").addClass("reveal");
      }

      // handling reveal on roadmap section
      $("ul.roadmap li").each(function () {
        const $this = $(this);
        let offset;
        if ($(window).width() < 1024) {
          offset = $this.height() * 2;
        } else {
          offset = $this.height() * 3.25;
        }

        const elScrollPos = $this.offset().top;
        if (bottomScrollPos > elScrollPos + offset) {
          $this.addClass("reveal");
        } else {
          $this.removeClass("reveal");
        }
      });
    });

    // Init Lazy Load Images
    if (typeof jQuery.Lazy == "function") {
      $(".lazy").Lazy({
        afterLoad: function (element) {
          if ($(element).hasClass("init-zoom")) {
            // Magnify Zoom on Image Preview
            $zoom = $(".large-preview img").magnify();
          }
        },
      });
    } else {
      console.log("no lazy");
    }
  }
});
