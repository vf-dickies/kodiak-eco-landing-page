// See http://brunch.io for documentation.
exports.files = {
  javascripts: {
    joinTo: {
      'vendor.js': /^(?!src)/, // Files that are not in `src` dir.
      'app.js': /\.js$/
    }
  },
  stylesheets: {joinTo: 'screen.css'}
};

exports.paths = {
  watched: ['src','vendor']
}

exports.plugins = {
  babel: {presets: ['latest']}
};
